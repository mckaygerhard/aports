# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=xmrig
pkgver=6.8.2
pkgrel=0
pkgdesc="XMRig is a high performance Monero (XMR) miner"
url="https://xmrig.com/"
arch="aarch64 x86 x86_64" # officially supported by upstream
license="GPL-3.0-or-later"
options="!check" # No test suite from upstream
makedepends="cmake libmicrohttpd-dev libuv-dev openssl-dev hwloc-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/xmrig/xmrig/archive/v$pkgver.tar.gz"

case "$CARCH" in
	aarch64*) CMAKE_CROSSOPTS="-DARM_TARGET=8"; makedepends="$makedepends linux-headers" ;;
esac

build() {
	cmake -B build $CMAKE_CROSSOPTS -DCMAKE_BUILD_TYPE=None \
		-DWITH_CUDA=OFF \
		-DWITH_OPENCL=OFF

	make -C build
}

package() {
	install -Dm 755 build/xmrig $pkgdir/usr/bin/xmrig

	install -Dm 644 -t "$pkgdir"/usr/share/doc/$pkgname/ README.md
}

sha512sums="5f4c356602dcf82d5ba95d1dcde12eb7a004d75543fd5703a9d986485d3229f87e98e26e7fb3c2a6eb14a268545e2f1545fb48541ca41b66ef352ce1a2a42be4  xmrig-6.8.2.tar.gz"
